package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppUser {
    String firstName;
    String lastName;
    String email;
    boolean enabled;
    String username;
    String password;

}
@AllArgsConstructor
@NoArgsConstructor
@Data
class Credentials {
    String type;
    String value;
    boolean temporary;
}
