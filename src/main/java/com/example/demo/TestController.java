package com.example.demo;

import com.example.demo.security.LoginRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class TestController {
    RestTemplate restTemplate;
    ObjectMapper objectMapper;

    @RolesAllowed("leo-rol")
    @GetMapping("/private")
    public String getTest(){
        return "this is private!";
    }
    @GetMapping
    public List<String> getAll(){
        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl("http://localhost:8081/auth/")
                .realm("master")
                .grantType(OAuth2Constants.PASSWORD)
                .clientId("admin-cli")
                .username("admin")
                .password("admin")
                .build();
        String adminToken = keycloak.tokenManager().getAccessToken().getToken();
        RealmResource realmResource = keycloak.realm("leo");
        UsersResource usersResource = realmResource.users();
        List<UserRepresentation> list = usersResource.list();
        return list.stream().map(s -> s.getId()).collect(Collectors.toList());
    }

    @PostMapping("/login")
    public AdminResponseToken login(@RequestBody LoginRequest loginRequest){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username",loginRequest.getUsername());
        map.add("password", loginRequest.getPassword());
        map.add("grant_type","password");
        map.add("client_id","leo-app");
        map.add("client_secret", "c95d1245-f320-4179-bc51-c64a41e95cfc");
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
        ResponseEntity<AdminResponseToken> adminResponseToken = restTemplate.exchange("http://localhost:8081/auth/realms/leo/protocol/openid-connect/token", HttpMethod.POST, entity, AdminResponseToken.class);
        System.out.println(adminResponseToken.getBody());
        return adminResponseToken.getBody();
    }
    @PostMapping("refresh-token")
    public AdminResponseToken refreshToken(@RequestHeader("refresh-token") String t){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type","refresh_token");
        map.add("client_id","leo-app");
        map.add("refresh_token", t);
        map.add("client_secret", "c95d1245-f320-4179-bc51-c64a41e95cfc");
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
        ResponseEntity<AdminResponseToken> adminResponseToken = restTemplate.exchange("http://localhost:8081/auth/realms/leo/protocol/openid-connect/token", HttpMethod.POST, entity, AdminResponseToken.class);
        System.out.println(adminResponseToken.getBody());
        return adminResponseToken.getBody();
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody AppUser appUser){
            Keycloak keycloak = KeycloakBuilder.builder()
                    .serverUrl("http://localhost:8081/auth/")
                    .realm("master")
                    .grantType(OAuth2Constants.PASSWORD)
                    .clientId("admin-cli")
                    .username("admin")
                    .password("admin")
                    .build();
        String adminToken = keycloak.tokenManager().getAccessToken().getToken();
        // Define user
        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(appUser.getUsername());
        user.setFirstName(appUser.getFirstName());
        user.setLastName(appUser.getLastName());
        user.setEmail(appUser.getEmail());
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType(OAuth2Constants.PASSWORD);
        credentialRepresentation.setValue(appUser.getPassword());
        user.setCredentials(Collections.singletonList(credentialRepresentation));
        RealmResource realmResource = keycloak.realm("leo");
        UsersResource usersResource = realmResource.users();
        try{
            Response response = usersResource.create(user);
            String userId = CreatedResponseUtil.getCreatedId(response);
            UserResource userResource = usersResource.get(userId);
            RoleRepresentation testerRealmRole = realmResource.roles()//
                    .get("user").toRepresentation();
            userResource.roles().realmLevel() //
                    .add(Arrays.asList(testerRealmRole));
            ClientRepresentation app1Client = realmResource.clients() //
                    .findByClientId("leo-app").get(0);
            RoleRepresentation userClientRole = realmResource.clients().get(app1Client.getId()) //
                    .roles().get("leo-rol").toRepresentation();
            System.out.println(1);
            userResource.roles() //
                    .clientLevel(app1Client.getId()).add(Arrays.asList(userClientRole));
            System.out.println(2);
            return ResponseEntity.status(response.getStatus()).body(response.getStatusInfo().getReasonPhrase());

        }catch (WebApplicationException ex){
            return ResponseEntity.status(ex.getResponse().getStatus()).body(ex.getMessage());
        }

    }
    @RolesAllowed("admin")
    @DeleteMapping("/delete/{userId}")
    public void deleteUser(@PathVariable("userId") String userId){
        Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl("http://localhost:8081/auth/")
                .realm("master")
                .grantType(OAuth2Constants.PASSWORD)
                .clientId("admin-cli")
                .username("admin")
                .password("admin")
                .build();
        String adminToken = keycloak.tokenManager().getAccessToken().getToken();
        RealmResource realmResource = keycloak.realm("leo");
        UsersResource usersResource = realmResource.users();
        UserResource userResource = usersResource.get(userId);
        userResource.remove();
    }


}
